export const MSG_WRONG_PASSWORD = "Wachtwoord is niet juist!";
export const MSG_NO_USER_FOUND =
  "Er bestaat geen gebruiker met deze gebruikersnaam.";
export const MSG_SOMETHING_WENT_WRONG = "Er ging iets mis!";
export const MSG_USERNAME_UNDEFINED = "Gebruikersnaam mag niet leeg zijn";
export const MSG_PASSWORD_UNDEFINED = "Wachtwoord mag niet leeg zijn";
export const MSG_NO_TOKEN = "Geen token aanwezig in Authorization header";

export function handleError(error) {
  const errorMessage = error.message;
  let statusCode;
  let message;

  if (errorMessage.includes(MSG_WRONG_PASSWORD)) {
    statusCode = 401;
    message = MSG_WRONG_PASSWORD;
  } else if (errorMessage.includes(MSG_USERNAME_UNDEFINED)) {
    statusCode = 400;
    message = MSG_USERNAME_UNDEFINED;
  } else if (errorMessage.includes(MSG_PASSWORD_UNDEFINED)) {
    statusCode = 400;
    message = MSG_PASSWORD_UNDEFINED;
  } else if (errorMessage.includes(MSG_NO_USER_FOUND)) {
    statusCode = 401;
    message = MSG_NO_USER_FOUND;
  } else if (errorMessage.includes(MSG_NO_TOKEN)) {
    statusCode = 401;
    message = MSG_NO_TOKEN;
  } else {
    statusCode = 500;
    message = MSG_SOMETHING_WENT_WRONG;
  }
  return { statusCode, message };
}
