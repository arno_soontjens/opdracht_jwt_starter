import { refreshContent } from "..";
import { persistTokenToStorage } from "./util";

export const API_URL = "http://localhost:8000";

function showLoginError(error) {
  //TODO: toon de alert en geef de error weer.
}

function storeTokenAndRefresh(token) {
  //TODO: sla de token op en refresh de index pagina (refreshContent()) daarna.
}

export function login(username, password) {
  //TODO
  // Voer een fetch request uit naar API_URL/login om de token op te halen, return de token
}

export function addLoginHandler() {
  //TODO: zorg dat je het id hier ofwel in je login.html pagina gebruikt, of vervang het hier door het id dat jij gebruikt.
  const loginButton = document.getElementById("btn_login");
  loginButton.addEventListener("click", (event) => {
    event.preventDefault();
    //TODO
    // Haal username en password uit het formulier
    login(username, password)
      .then((result) => {
        console.log("Result:", result);
        //TODO:
        //storeTokenAndRefresh(result.token);
      })
      .catch((error) => showLoginError(error));
  });
}
